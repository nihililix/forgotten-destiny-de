/**
 * Webpack Config
 */
const Path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: {
        app: Path.resolve(__dirname, '../src/scripts/index.js'),
    },
    output: {
        // path to webpack output
        path: Path.join(__dirname, '../www/assets/'),
        filename: 'js/[name].js',
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            name: false,
        },
    },
    plugins: [
        // Copy things, only images
        new CopyWebpackPlugin({
            patterns: [{
                from: Path.resolve(__dirname, '../src/assets/images'),
                to: '../assets/images/'
            }]
        }),
    ],
    resolve: {
        alias: {
            '~': Path.resolve(__dirname, '../src'),
        },
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                include: Path.resolve(__dirname, '../src'),
                include: /node_modules/,
                enforce: 'pre',
                loader: 'eslint-loader',
                options: {
                emitWarning: true
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                include: Path.resolve(__dirname, '../src'),
                loader: 'babel-loader'
            },
            {
                test: /\.s?css/i,
                use : [
                MiniCssExtractPlugin.loader,
                'css-loader',
                {
                    loader: 'postcss-loader',
                    options: {
                        ident: 'postcss',
                        plugins: [
                            require('autoprefixer')({})
                        ]
                    }
                },
                'sass-loader'
                ]
            },
            {
                test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'misc'
                    },
                },
            }
        ],
    },
};
